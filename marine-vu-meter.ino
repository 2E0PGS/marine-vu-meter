/*
Copyright (C) <2019-2020>  <Peter Stevenson> (2E0PGS)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// 490Hz square wave. Call it 50% duty so factor that in.

//analogWrite(11, 70); // 1.3V (8 KA on meter)
//analogWrite(11, 60); // 1.1V
//analogWrite(11, 50); // 976mv
//analogWrite(11, 40); // 700mv?
//analogWrite(11, 30); // 600mv?
//analogWrite(11, 20); // 400mv
//analogWrite(11, 17); // 300mv
//analogWrite(11, 13); // 250mv
//analogWrite(11, 10); // 200mv
//analogWrite(11, 6); // 120mv
//analogWrite(11, 3); // 60mv

int maxVolSeen = 10;
//int scaleTimer = 0;
int sensitivity = 10;
int debugLevel = 0;

int leftChannelMeterMaxLevel = 70;
int leftChannelMeterMinLevel = 0;
int leftChannelMeterPin = 11;
int leftChannelInputPin = 0;
int leftChannelLastValue = 0;

int rightChannelMeterMaxLevel = 70; // Keeps changing it's ohms.
int rightChannelMeterMinLevel = 0;
int rightChannelMeterPin = 10;
int rightChannelInputPin = 5;
int rightChannelLastValue = 0;

void setup() {
  if (debugLevel > 0) {
    Serial.begin(9600);
  }

  // Startup meter test.
  int i = 0;
  while (i < 70) {
    delay(20);
    i++;
    if (debugLevel > 3) {
      Serial.println(i);
    }
    analogWrite(leftChannelMeterPin, i);
    analogWrite(rightChannelMeterPin, i);
  }
  while (i > 0) {
    delay(20);
    i--;
    if (debugLevel > 3) {
      Serial.println(i);
    }
    analogWrite(leftChannelMeterPin, i);
    analogWrite(rightChannelMeterPin, i);
  }
  
  // Neddle drop speed test.
  analogWrite(leftChannelMeterPin, leftChannelMeterMaxLevel);
  analogWrite(rightChannelMeterPin, rightChannelMeterMaxLevel);
  delay(1000);
  analogWrite(leftChannelMeterPin, leftChannelMeterMinLevel);
  analogWrite(rightChannelMeterPin, rightChannelMeterMinLevel);
  delay(1000);
}

void loop() {
  // Read.
  int leftChannelReadVal = analogRead(leftChannelInputPin);
  int rightChannelReadVal = analogRead(rightChannelInputPin);
  if (debugLevel > 0) {
    Serial.println("Left channel read: " + String(leftChannelReadVal));
    Serial.println("Right channel read: " + String(rightChannelReadVal));
  }

  // Map.
  int leftChannelMappedVal = map(leftChannelReadVal, 0, 100, leftChannelMeterMinLevel, leftChannelMeterMaxLevel);
  int rightChannelMappedVal = map(rightChannelReadVal, 0, 100, rightChannelMeterMinLevel, rightChannelMeterMaxLevel);
  if (debugLevel > 1) {
    Serial.println("Left channel mapped: " + String(leftChannelMappedVal));
    Serial.println("Right channel mapped: " + String(rightChannelMappedVal));
  }

  // Left channel meter needle protection.
  if (leftChannelMappedVal > leftChannelMeterMaxLevel) {
    analogWrite(leftChannelMeterPin, leftChannelMeterMaxLevel);
  }
  else if (leftChannelMappedVal < leftChannelMeterMinLevel) {
    analogWrite(leftChannelMeterPin, leftChannelMeterMinLevel);
  }
  else {
    if (leftChannelLastValue - leftChannelMappedVal > sensitivity || leftChannelLastValue - leftChannelMappedVal < -sensitivity) {
     analogWrite(leftChannelMeterPin, leftChannelMappedVal);
     if (debugLevel > 2) {
       Serial.println("Left channel difference: " + String(leftChannelLastValue - leftChannelMappedVal));
     }
     leftChannelLastValue = leftChannelMappedVal;
    }
  }

  // Right channel meter needle protection.
  if (rightChannelMappedVal > rightChannelMeterMaxLevel) {
    analogWrite(rightChannelMeterPin, rightChannelMeterMaxLevel);
  }
  else if (rightChannelMappedVal < rightChannelMeterMinLevel) {
    analogWrite(rightChannelMeterPin, rightChannelMeterMinLevel);
  }
  else {
    if (rightChannelLastValue - rightChannelMappedVal > sensitivity || rightChannelLastValue - rightChannelMappedVal < -sensitivity) {
     analogWrite(rightChannelMeterPin, rightChannelMappedVal);
     if (debugLevel > 2) {
       Serial.println("Right channel difference: " + String(rightChannelLastValue - rightChannelMappedVal));
     }
     rightChannelLastValue = rightChannelMappedVal;
    }
  }

  // Max seen.
  if (leftChannelReadVal > maxVolSeen) {
    maxVolSeen = leftChannelReadVal;
  }
  else if (rightChannelReadVal > maxVolSeen) {
    maxVolSeen = rightChannelReadVal;
  }
  //else{
  //  if (maxVolSeen > 10 && scaleTimer > 10) {
  //    maxVolSeen--;
  //    scaleTimer = 0;
  //  }
  //}
  if (debugLevel > 2) {
    Serial.println("Max loudless: " + String(maxVolSeen));
  }
}
